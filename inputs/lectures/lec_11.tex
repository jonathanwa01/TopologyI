%! TEX root = ../../master.tex
\lecture[]{Sa 20 Nov 2021}{Untitled}

\begin{orga}
    The presentation for the Bachelor thesis will be on Monday, 13$^{\text{th}}$ of december.
\end{orga}

\begin{proof}
    Since $H_1^{\sing}(X)$ is abelian, by the universal property of abelianization, $h$ factors through $\pi_1(X,x)^{\ab}$ as claimed. It remains to show that this map is an isomorphism.

    For each $y\in X$ pick a path $w_y$ from $y$ to  $x$. Take  $w_x = c_x$ as the constant path.

    \underline{Surjectivity:} Let $a\in H_1^{\sing}(X)$ be represented by $\sum_{i=1}^n r_iv_i$ with $r_i\in \mathbb{Z}$ and $v_i\colon I \to X$.

    \missingfigure{Illustration of loops in $X$}

    Define $\tilde{v}_i\colon w_{v_i(0)}^{-1} \star v_i \star w_{v_i(1)}$ and view these as $[\tilde{v}_i]\in \pi_1(X,x)$.

    \begin{claim}
        We have that $h\left( \prod_{i=1}^n [\tilde{v}_v]^{r_i} \right)  = a\in H_1^{\sing}(X)$
    \end{claim}
    \begin{subproof}
        \begin{IEEEeqnarray*}{rCl}
            h\left( \prod_{i=1}^n [\tilde{v}_i]^{r_i} \right) & = & \sum_{i=1}^n r_i \cdot [\tilde{v}_i]\\
                                                              & = & \sum_{i=1}^n r_i\left( [w_{v_i(0)}^{-1}] + [v_i] + [w_{v_i(1)}] \right) \\
                                                              & = & \sum_{i=1}^n r_i\left(-[w_{v_i(0)}] + [v_i] + [w_{v_i(1)}]\right) \\
                                                              & = & \sum_{i=1}^n r_iv_i + \sum_{i=1}^n r_i(w_{v_i(1)} - w_{v_i(0)})
        \end{IEEEeqnarray*}
        But since
        \[
            0 = \partial\left(\sum_{i=1}^n r_iv_i\right) = \sum_{i=1}^n r_i \left(v_i(1) - v_i(0)\right)
        \]
        the last summand cancels and we established our desired result.
    \end{subproof}

        \underline{Injectivity:} We define a map by
            \begin{equation*}
            \varphi: \left| \begin{array}{c c l} 
                C_1^{\sing}(X) & \longrightarrow & \pi_1(X,x)^{\ab} \\
                v\colon \Delta^1 \to x & \longmapsto &  w_{v(0)}^{-1}\star v \star w_{v(1)}
            \end{array} \right.
        \end{equation*}
        For $z\colon \Delta^2 \to X$, let $\partial z = τ_0 - τ_1 + τ_2$, we want to show $\varphi(\partial z) = 0 \in \pi_1(X,x)^{\ab}$.
        We have
        \[
            \varphi(\partial z) = \tilde{τ}_0 - \tilde{τ}_1 + \tilde{τ}_2  
        \] 
        so that
        \[
            \tilde{τ}_2 \star \tilde{τ}_0 \simeq w_{z(0)}^{-1} \star τ_2  \star τ_0 \star w_{z(2)} \simeq w_{z(0)}^{-1} \star τ_1 \star w_{z(2)} = \tilde{τ}_1 
        \]
        so in fact $\varphi(\partial z) = 0\in \pi_1(X,x)^{\ab}$.

        So $\varphi$ factors through $\faktor{C_1^{\sing(X)}}{\im \partial} \to \pi_1(X,x)^{\ab}$ and this restricts to a homomprhism
        \[
            \tilde{\varphi}\colon  H_1^{\sing}(X) \to \pi_1(X,x)^{\ab} 
        \]

        By definition, we now have $\varphi \circ  h = \id_{\pi_1(X,x)^{\ab}}$ as
        \[
            \tilde{\varphi} \circ h(α) = [c_x^{-1} \star α \star c_x] = [α] 
        \]
        So $h$ is injective, since it has a left inverse.
\end{proof}

\begin{lemma}\label{lm:coproduct-property-of-homology-extends-to-pairs}
    If for an index set $I$, the natural map
    \[
        \bigoplus_{i \in I} H_n(X_i) \to H_n\left(\coprod _{i \in I}X_i\right)
    \] 
    is an isomorphism for all families $\left\{X_i\right\} _{i \in I}$, then also for all families of pairs $\left\{(X_i, A_i)\right\}_{i \in I} $,
    \[
        \bigoplus _{i \in I} H_n(X_i, A_i) \to H_n\left(\coprod X_i, \coprod A_i\right)
    \]
    is an isomorphism.
\end{lemma}

\begin{proof}
    Consider the pair sequences
    \[
    \begin{tikzcd}
        \ar{r}{\partial} & \ar{d}  \bigoplus _{i \in I}H_n(A_i) \ar{r} & \ar{d} \bigoplus _{i \in I}H_n(X_i) \ar{r} & \ar{d} \bigoplus_{i \in I}H_n(X_i,A_i) \ar{r}{\partial} &  \ldots \\
\ar{r}{\partial} &  H_n\left( \coprod_{i \in I}A_i \right) \ar{r} & H_n(\coprod_{i \in I}X_i) \ar{r} & H_n(\coprod_{i \in I}X_i, \coprod_{i \in I}A_i) \ar{r}{\partial} &  \ldots
    \end{tikzcd}
\]
and we can just apply the \nameref{lm:five-lemma} once again.
\end{proof}


\section{CW-complexes}

\begin{definition}\label{def:relative-cw-complex-structure}
    Let $(X,A)$ be a pair of spaces. Let  $A$ be a Hausdorff space. A  \vocab{relative CW-structure} on $(X,A)$ is a filtration
    \[
    A = X_{-1} \subset X_0 \subset X_1 \subset \ldots \subset X
\]
such that
\begin{enumerate}[p]
    \item For all $n\in \mathbb{N}$ there exists a pushout
        \[
        \begin{tikzcd}
\pushout            \coprod_{i \in I}S^{n-1} \ar[swap]{d}{\coprod_{i \in I}j_i} \ar{r}{\coprod_{i \in I}q_i^n} & X_{n-1} \ar{d}{} \\
            \coprod _{i \in I}D^n \ar[swap]{r}{\coprod_{i \in I}Q_i^n} & X_n
        \end{tikzcd}
        \]
    \item $X = \bigcup_{n\geq -1}X_n $ and $X$ has the colimit topology.
\end{enumerate}
We call $(X,A)$ with a relative  $CW$-structure a  \vocab{relative $CW$-complex}.
If $A = \emptyset$, we call $X$ a  \vocab{$CW$-complex}. 

If $A$ is a  $CW$-complex, we call the relative  $CW$-complex  $(X,A)$ a  $CW$-pair.
\end{definition}

\begin{example*}
From a space $A$ we can incrementally construct a bigger space:
     \missingfigure{Illustration of relative $CW$-structure}
\end{example*}

\begin{remark}
    \begin{enumerate}[1.]
        \item  The colimit topology (or direct limit topology, or weak topology) is the topology such that
    \[
    f\colon X \to Y
    \]
    is continuous if and only if $\frestriction{f}{X_n}$ is continuous for all $n\in \mathbb{N}$, i.e. $(ii)$ in  \autoref{def:relative-cw-complex-structure} is equivalent to saying that $X$ is the colimit of 
    \[
    X_{-1} \hookrightarrow X_0 \hookrightarrow  X_1 \hookrightarrow  X_2 \hookrightarrow \ldots
    \] 
\item $X_n$ is called the \vocab{$n$-skeleton} of  $X$.
    The path-components of $X_n \setminus X_{n-1}$ are the \vocab{open $n$-cells}.
    For a choice of $Q_i^n$ as in (i), we we get a homeomorphism
     \[
         \coprod_{i \in I} (D^n \setminus S^{n-1}) \stackrel{\cong}{\longrightarrow} X_n \setminus X_{n-1}
    \] 
    The \vocab{closed $n$-cells} are the closures of the open $n$-cells in $X_n$. 
    They are homeomorphic to $Q_i^n(D^n)$, but not to  $D^n$ in general.
\item The $q_i^n, Q_i^n$ in  \autoref{def:relative-cw-complex-structure} are a choice and not part of the data.
    If they are chosen, we call $Q_i^n$ the \vocab{characteristic map} of the  $n$-cell, and $q_i^n$ the  \vocab{attaching map}. 
    \end{enumerate}
\end{remark}


\begin{example}
    On $S^n$, we have the $CW$-structure.
     \[
    X_{-1}=\emptyset, X_0 = \ldots = X_{n-1} = \star, X_n = S^n
    \]
    and implicitly, $X_{n+1} = X_{n+2} = \ldots = S^n$ as well, with the pushout
    \[
    \begin{tikzcd}
\pushout        S^{n-1} \ar[swap]{d}{} \ar{r}{} & \star \ar{d}{} \\
        D^n \ar[swap]{r}{} & S^n
    \end{tikzcd}
    \]
    A different $CW$-structure on  $S^n$ is given by  $X_k = S^k$ for all  $k\leq n$, inductively constructed by the pushout
    \[
    \begin{tikzcd}
\pushout        S^{k-1}\coprod S^{k-1} \ar[swap]{d}{} \ar{r}{\id \coprod \id} & S^{k-1} \ar{d}{} \\
        D^k \coprod D^k \ar[swap]{r}{} & S^k
    \end{tikzcd}
    \]

    \missingfigure{CW-complex structure on $S^n$}

\end{example}

\begin{example}
    \begin{enumerate}[1.]
        \item There is a CW-structure on $X = \mathbb{R}\mathbb{P}^n$ such that $X_k = \mathbb{R}\mathbb{P}^k$ for $k\leq n$.
            
            To see this, recall that 
            \[
                \mathbb{R}\mathbb{P}^n \cong \faktor{D^n}{x \sim -x} \qquad \forall x\in S^{n-1}
            \] 
            \[
            \mathbb{R}\mathbb{P}^{n-1} \cong \faktor{S^{n-1}}{x\sim -x}\qquad \forall x\in S^{n-1}
            \]
            Hence there is a pushout
            \[
            \begin{tikzcd}
                S^{n-1} \ar[swap]{d}{} \ar{r}{q} & \mathbb{R}\mathbb{P}^{n-1} \ar{d}{} \\
                D^n \ar[swap]{r}{} & \mathbb{R}\mathbb{P}^n
            \end{tikzcd}
            \]

            We define
            \[
            \mathbb{R}\mathbb{P}^{\infty}\coloneqq \colim \mathbb{R}\mathbb{P}^n
            \] 
            This has a CW-structure with $n$-skeleton  $\mathbb{R}\mathbb{P}^{n}$.

            Similarly, one can also define
            \[
            S^{\infty}\coloneqq \colim S^n
            \] 
        \item We can similarly define $\mathbb{C}\mathbb{P}^n$ as the space of 1-dimensional complex subspaces of $\mathbb{C}^{n+1}$.
            As for $\mathbb{R}\mathbb{P}^n$, we have the following topology:

            $S^1 \subset \mathbb{C}$ acts on $S^{2n+1}\subset \mathbb{C}^{n+1}$ by scalar multiplication, and we define
            \[
                \mathbb{C}\mathbb{P}^{n} \coloneqq  \faktor{S^{2n+1}}{S^1}
            \] 
            with the quotient topology.
            Note that the quotient is really the group action, not a subspace.

            Since this $S^1$-action keeps $S^{2n-1}\subset S^{2n+1}$ invariant, we get an inclusion
            \[
            \mathbb{C}\mathbb{P}^{n-1}\subset \mathbb{C}\mathbb{P}^n
            \] 
    \end{enumerate}
\end{example}

\begin{lemma}\label{lm:cw-complex-of-complex-projective-space}
    The diagram
    \[
    \begin{tikzcd}
        S^{2n-1} \ar[swap]{d}{} \ar{r}{} & \faktor{S^{2n-1}}{S^1} \ar{d}{} \\
        D^{2n} \ar[swap]{r}{} & \mathbb{C}\mathbb{P}^n
    \end{tikzcd}
    \]
    is a pushout. Hence $\mathbb{C}\mathbb{P}^n$ is a $CW$-complex with one cell in each even dimension  $\leq 2n$, and no cells in odd dimensions.
\end{lemma}

\begin{proof}
    For $x\in \mathbb{C}\setminus \left\{0\right\} $, there exists a unique $λ\in S^1$ such that $λ\cdot x \in \mathbb{R}_{>0}$. Consider the inclusion
    \[
        \left\{(x, \sqrt{1-\norm{x} } \in \mathbb{C}^n \times \mathbb{C} \mid  \norm{x} \leq 1\right\} \hookrightarrow S^{2n+1} \twoheadrightarrow \faktor{S^{2n+1}}{S^1}\cong \mathbb{C}\mathbb{P}^n
    \] 
    By the above, the composition is surjective and induces a continuous bijection
    \[
        \faktor[.9]{\left\{(x, \sqrt{1-\norm{x}})\in \mathbb{C}^n \times \mathbb{C}\mid  \norm{x} =1 \right\}}[-0.7]{\substack{(x,0)\sim (λx,0)\\\forall λ\in S^1} }\stackrel{f}{\longrightarrow}  \mathbb{C}\mathbb{P}^n
    \] 

    But this is now a continuous bijection from a compact into a Hausdorff space, so that this is a homeomorphism.
    So the map
        \begin{equation*}
        \begin{array}{c c l} 
            domain & \longrightarrow & \faktor{D^{2n}}{\substack{x\sim λx\\ \forall x\in S^{2n-1}, λ\in S^1} } \\
            (x, \sqrt{1 - \norm{x} }) & \longmapsto &  x 
        \end{array}
    \end{equation*}
    is also a homeomorphism.

    Thus, our claimed diagram is indeed a pushout.
\end{proof}
