%! TEX root = ../../master.tex
\lecture[Motivation for the lecture. Five Lemma. Component-wise homotopy equivalence of pairs induce homology isomorphisms. Distinction of $\mathbb{R}^n,\mathbb{R}^m$ for $n\neq m$. Excisive triad. The Mayer-Vietories sequence.]{Do 21 Okt 2021}{Untitled}

\begin{oral}
    Since the lecture was quite fast up to now, we will have a quick summary of what this lecture is about and  what we have done so far.
\end{oral}

\begin{moral}
    We want to find and study an algebraic (homotopy) invariant of spaces that is 'easy' to compute.
\end{moral}

The main example for this is \emphasize{homology theory}. A homology theory can be defined abstractly via axioms. To prove the existence of such a theory, we consider singular homology.

\begin{oral}
    For the most part of the lecture, we actually do not care about singular homology concretely and talk about 'a' homology theory. The only import fact to keep in mind is that singular homology theory is an example of a homology theory, so there exists at least one.
\end{oral}

\begin{question}
    Why did we consider simplicial homology and semi-simplicial sets at all?
    \begin{enumerate}[1.]
        \item To motivate why we define singular homology the way we did
        \item To already have computable examples of homology.
    \end{enumerate}
\end{question}

\section{Consequences of the axioms}

\begin{lemma}\label{lm:homology-of-b-b-is-trivial}
    Let $B$ be a space. Then
     \[
         H_n(B,B) = 0 \qquad \forall n\in \mathbb{N}
    \] 
\end{lemma}
\begin{proof}
    By the LES of the pair $(B,B)$ we get an exact sequence

    \begin{tikzcd}
        \ar{r} &         H_n(B) \ar{r}{\id_*} &   H_n(B) \ar{r}{i_*}   &  H_n(B,B) \ar{r}{\partial}  & H_{n-1}(B) \ar{r}{\id_*} &  H_{n-1}(B)
    \end{tikzcd}

    Since $\id$ is surjective,  $i_*$ is the zero map. But also since $\id$ is injective,  $\partial$ is the zero map. But then
    \[
        H_n(B,B) = \ker \partial = \im i_* = 0
    \] 
\end{proof}

\begin{lemma}[Five Lemma]\label{lm:five-lemma}
    Consider a commutative diagram of abelian groups (or $R$-modules) with exact rows

\[
    \begin{tikzcd}
        M_1 \ar{r}\ar{d}{f_1} & M_2 \ar{r} \ar{d}{f_2}&  M_3 \ar{r} \ar{d}{f_3}& M_3 \ar{r}\ar{d}{f_3} &  M_5 \ar{d}{f_5}\\
        N_1 \ar{r} & N_2 \ar{r} & N_3 \ar{r} &  N_3 \ar{r} &  N_5
    \end{tikzcd}
\]
Suppose that $f_1,f_2,f_4,f_5$ are all isomorphisms. Then $f_3$ is an isomorphism.
\end{lemma}
\begin{proof}
    Given as an exercise.
\end{proof}


\begin{remark}
    Actually, a weaker assumption on $f_1,f_5$ suffices (see the exercise).
\end{remark}

\begin{oral}
    It is actually of great importance that $f_3$ is given already, even if not assuming any properties. There are situations where $f_1,f_2,f_4,f_5$ are isomorphisms, but there exists no $f_3$ making the diagram commute.
\end{oral}

\begin{lemma}\label{lm:homotopy-equivalences-on-space-and-subspace-induce-isomorphisms}
    Let $f\colon (X,A) \to  (Y,B)$ be a map of pairs such that $f\colon X \to  Y$ and $\frestriction{f}{A}\colon A \to B$ are homotopy equivalences. Then
    \[
        f_*\colon H_n(X,A) \to H_n(Y,B)
    \] 
    is an isomorphism.
\end{lemma}

\begin{recap}
    Note that this is strictly weaker that $f$ is a homotopy equivalence of pairs: Consider e.g. the map
     \[
         (S^n,S^{n-1})\stackrel{\id}{\longrightarrow} (S^n, S^n \setminus \left \{N,S\right\} )
    \] 
    that is not a homotopy equivalence of pairs (there is not even a map of pairs in the other direction), but it satisfies the assumptions of \autoref{lm:homotopy-equivalences-on-space-and-subspace-induce-isomorphisms}.
\end{recap}

\begin{proof}
    If $g\colon V \to  W$ is a homotopy equivalence with inverse $g'\colon W \to  V$, then
    \[
        H_n(g) \circ  H_n(g') = H_n(g \circ  g') \stackrel{\text{homotopy inv.}}{=} H_n(\id_W) = \id_{H_n(W)}
    \] 
    Also
    \[
        H_n(g') \circ H_n(g) = H_n(g' \circ g) =H_n(\id_V) = \id_{H_n(V)}
    \] 
    Thus $H_n(g)$ is injective and surjective, so it is an isomorphism (and in particular, $H_n(g')$ is its inverse). Now consider the LES of pairs

\[
\begin{tikzcd}
    \ar{r}{\partial} &  H_n(A) \ar{r}{} &  H_n(X) \ar{r} & H_n(X,A) \ar{r}{\partial} &  H_{n-1}(A) \ar{r}{} &  H_{n-1}(X) \\
    \ar{r}{\partial} &  H_n(B) \ar{r} & H_n(Y) \ar{r} & H_n(Y,B) \ar{r} & H_{n-1}(B) \ar{r} & H_{n-1}(Y)
    \ar[from=1-2, to =2-2, "\frestriction{f}{A}_*"]
    \ar[from=1-2, to =2-2, swap ,"\cong"]
    \ar[from=1-3, to =2-3, "f_*"]
    \ar[from=1-3, to =2-3, swap ,"\cong"]
    \ar[from=1-4, to =2-4, "f_*"]
    \ar[from=1-5, to =2-5, "f_*"]
    \ar[from=1-5, to =2-5, swap, "\cong"]
    \ar[from=1-6, to =2-6, "\frestriction{f}{A}_*"]
    \ar[from=1-6, to =2-6, swap, "\cong"]
\end{tikzcd}
\]

By the first part of the proof, we conclude that $f, \frestriction{f}{A}$ are isomorphisms on $H_n(A)$, $ H_n(X)$, $H_{n-1}(A)$, $H_{n-1}(X)$. Thus, by the  \nameref{lm:five-lemma} we conclude that
\[
    f_*\colon H_n(X,A) \to H_n(Y,B)
\]
also is an isomorphism.
\end{proof}

\begin{theorem}\label{thm:r-n-are-pairwise-distinct}
    $\forall d,e\in \mathbb{N}$, $d,e\geq 1$, the following are equivalent:
    \begin{enumerate}[e]
        \item $\mathbb{R}^d \cong \mathbb{R}^e$
        \item $S^{d-1} \simeq S^{e-1}$ 
        \item $d=e$
    \end{enumerate}
\end{theorem}

\begin{proof}
    $(1) \implies (2)$. Let $f\colon \mathbb{R}^d \to  \mathbb{R}^e$ be a homomorphism, then $\frestriction{f}{}$ is still a homeomorphism, so that
    \[
       S^{d-1}\simeq \mathbb{R}^d \setminus \left \{0\right\} \stackrel{\frestriction{f}{}}{\longrightarrow} \mathbb{R}^e \setminus \left \{f(0)\right\}  \simeq S^{e-1}
    \] 
    are homotpy equivalent.

    $(2) \implies(2)$. By \autoref{lm:homotopy-equivalences-on-space-and-subspace-induce-isomorphisms} we deduce that
    \[
        H_k(S^{d-1}) \cong H_k(S^{e-1}) \qquad \forall k\in \mathbb{N}
    \] 
    so by \autoref{thm:homology-of-unit-sphere-for-homology-theory-with-dimension-axiom} we get that in fact $d=e$.

    '$(3) \implies (1)$' is trivial.
\end{proof}

\begin{definition}[Excisive triad]\label{def:excisive-triad}
    An \vocab{excisive triad} $(X,A,B)$ is a space  $X$ together with subpsaces  $A,B$ such that  $X = A\cup B$ such that the inclusion
    \[
        (A,A\cap B) \to (X,B)
    \] 
    induces an isomorphism
    \[
        H_n(A,A\cap B) \stackrel{\cong}{\longrightarrow} H_n(X,B) \qquad \forall n\in \mathbb{Z}
    \] 
\end{definition}

\begin{example}\label{ex:excisive-triad}
    Let $A,B\subset X$ be open and $X = A\cup B$, then $(X,A,B)$ is excisive.
\end{example}
\begin{proof}
    Observe that $A^c \coloneqq X\setminus A\subset X$ is closed and $\overline{A^c} = A^c \subset B = \mathring{B}$. Then the inclusion
    \[
        (A,A\cap B) = (X\setminus A^c, B\setminus A^c) \to (X,B)
    \] 
    induces an isomorphism by the excision axiom.
\end{proof}

\begin{editor}
   We state the adapted version of \autoref{thm:mayer-vietoris} here that was given at the beginning of lecture 9.
   However, the proof is not yet fully adapted, i.e. $i_1,i_2,j_1,j_2$ are used instead of $f$.
\end{editor}

\begin{theorem}[Mayer-Vietoris]\label{thm:mayer-vietoris}
    Let $(X_1,X_0) \stackrel{f}{\longrightarrow} (X,X_2)$ be an $H_*$-isomorphism.
    Then there is a natural long exact sequence, the \vocab{Mayer-Vietories sequence}
    \[
    \begin{tikzcd}
        \ar{r}{\partial} &  H_n(X_0) \ar{r}{i_1\oplus f_*} &  H_n(X_1) \oplus H_n(X_2) \ar{r}{f_*-j_*} &  H_n(X) \ar{r}{\partial} &  H_{n-1}(X_0)
    \end{tikzcd}
    \] 
    where $i,j$ are induced by the inclusions and
    \[
    \begin{tikzcd}
        \partial\colon H_n(X) \ar{r}{k} &  H_n(X,X_2) & H_n(X_1,X_0) \ar{l}[swap]{f_*}{\cong} \ar{r}{\partial} &  H_{n-1}(X_0)
    \end{tikzcd}
\]
where $k,l$ are induced by the inclusions (note that  $l$ is an isomorphism by the assumption in  \autoref{def:excisive-triad}) and $\partial$ is the boundary map in the pair sequence for $(X_1,X_0)$.
\end{theorem}

\begin{remark*}
    Naturality here means that for
    \[
    \begin{tikzcd}
        (X_1,X_0) \ar[swap]{d}{g} \ar{r}{f} & (X,X_2) \ar{d}{h} \\
        (X_1',X_0') \ar[swap]{r}{f'} & (X',X_2')
    \end{tikzcd}
    \]
    for two $H_*$-isomorphisms  $f,f'$, then there is an induced map of the corresponding Mayer-Vietories sequences.
\end{remark*}

\begin{oral}\label{oral:mayer-vietories-for-s-n}
    One can also use the Mayer-Vietoris sequence to compute the homology of $S^n$, by setting  $X_1 = S^n \setminus \left \{S\right\} $ and $X_2 = S^n \setminus \left \{N\right\} $.

    Check this as a good exercise yourself.
\end{oral}

\begin{proof}
    Consider the commutative diagram of pair sequences
\[
    \begin{tikzcd}
        \ar{r}{\partial} &  H_n(X_0) \ar{r}{i_1} &  H_n(X_1) \ar{r}{k'} &  H_n(X_1,X_0) \ar{r}{\partial} &  H_{n-1}(X_0) \ar{r}{} & \ldots\\
        \ar{r}{\partial} &  H_n(X_2) \ar{r}{j_2} &  H_n(X) \ar{r}{k} &  H_n(X,X_2) \ar{r}{\partial} &  H_{n-1}(X_2) \ar{r} & \ldots
    \ar[from=1-2, to=2-2,"f_*"]
    \ar[from=1-3,to=2-3,"f_*"]
    \ar[from=1-4,to=2-4, "f_*"]
    \ar[from=1-4,to=2-4, swap, "\cong"]
    \ar[from=1-5, to=2-5, "f_*"]
    \end{tikzcd}
\]

\begin{editor}
    Unfortunately, I did not (yet) have the time to complete above commutative diagram. Feel free to fix this and send a pull request on GitLab.
\end{editor}
\todo{finish this commutative diagram}

\todo{change to $f_*$ in the proof.}

Thus, $\partial \circ j_1 = \partial \circ  j_2 = 0, i_1\circ \partial=i_2\circ \partial=0$ and also $j_1\circ i_1=j_2\circ i_2$ so that $(j_1-j_2) \circ (i_2 \oplus i_2) = 0$

So we already know that the Mayer-Vietoris sequence is a complex. It remains to show exactness.

\begin{enumerate}[1.]
    \item We show exactness at $H_n(X_1) \oplus H_n(X_2)$, so suppose that $(j_1-j_2)(a,b) = 0$, i.e. $j_1(a) = j_2(b)$ for some $(a,b) \in H_n(X_1) \oplus H_n(X_2)$. Then
        \[
            l \circ  k'(a) = k \circ  j_1(a) = \underbrace{k \circ j_2}_{=0}(b) = 0
        \] 
        so we conclude that $k'(a) = 0$. Thus by exactness of the above LES, we conclude that there exists some  $a'\in H_n(X_0)$ such that $i_2(a') = a$. But also
        \[
            j_2 i_2(a') = j_1i_1(a') = j_1(a) = j_2(b)
        \] 
        Thus by exactness at $H_n(X_2)$ in the bottom LES, there exists some $b'\in H_{n+1}(X,X_2)$  with $\partial(b') = b-i_2(a') \in H_n(X_2)$. Then set
        \[
            c\coloneqq  \partial(l^{-1}(b')) + a' \in H_n(X_0)
        \]
        Then
        \[
            i_1(c) = i_1(a') + \underbrace{i_1(\partial}_{=0}(l^{-1}(b^{-1})) = i_1(a') = a
        \] 
        and
        \begin{IEEEeqnarray*}{rCl}
            i_1(c) &= &i_2(a') + \underbrace{i_2(\partial}_{=\partial \circ l}(l^{-1}(b'))) \\
                   & = & i_2(a') + \partial(b') \\
                   & = & i_2(a') + b - i_2(a') \\
                   & = & b
        \end{IEEEeqnarray*}
        so that we have found a preimage $c$ of  $b$ under  $i_1$ and are done.
    \item We show exactness at $H_n(X)$. Pick some $x\in H_n(X)$ with $\partial(x) = 0$. Then by definition, there is $y\in H_n(X_1,X_0)$ with $l(y) = k(x)$, and  $y\mapsto 0$ under $\partial$. So by exactness in the top row we can pick some $a\in H_n(X_1)$ with $a\stackrel{k'}{\longrightarrow} y$ so that we get
        \[
            j_1(a) -x \stackrel{k}{\longmapsto} 0 
        \] 
        since $k(j_1(a)) = l(k'(a)) = l(y) = k(x)$ by commutativity. So by exactness of the bottom row, now there exists some $b\in H_n(X_2)$ with $j_2(b) = j_1(a) - x$. Then we get
        \begin{IEEEeqnarray*}{rCl}
            (j_1-j_2)(a,b)& = & j_1(a) - j_2(b)\\
                          &=& j_1(a) - j_1(a) + x \\
                          &=& x
        \end{IEEEeqnarray*}
        so we conclude that $x\in \im j_2$ as desired. 

        \missingfigure{Diagram chasing diagram for part 2 of the proof}
    \item It remains to show exactness at $H_n(X_0)$. Consider some $c\in H_n(X_0)$ with $i_1(c) = 0$ and $i_2(c) =0$. Then by exactness of the top row, we find $c'\in H_{n+1}(X_1,X_0)$ that is a preimage of $c$ under  $\partial$. But then
        \[
            \partial(l(c')) = i_2(\partial(c')) = i_2(c) = 0
        \] 
        so that by exactness at $H_{n+1}(X,X_2)$ there exists some $x$ with  $\partial(x) = l(c)$. Thus
        \[
        \partial x = c
        \] 
        as desired.
\end{enumerate}
\end{proof}

\begin{remark}
    Similarly to Seifert van Kampen for $\pi_1$, Mayer-Vietories allows us to decompose a space into smaller pieces to compute its homology. See also \autoref{oral:mayer-vietories-for-s-n}.
\end{remark}
